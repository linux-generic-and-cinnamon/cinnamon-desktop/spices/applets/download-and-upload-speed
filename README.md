# Download and upload speed
An applet that shows usage of a network interface for Linux Mint Cinnamon

## About
- Original code by **cardsurf** at [Gitlab](https://gitlab.com/cardsurf/download-and-upload-speed)  updated at [Cinnamon Spices](https://cinnamon-spices.linuxmint.com/applets/view/238)  
- Fork by **Drugwash**:  
-- Added tooltip as alternative to the hover popup which misbehaves in Linux Mint < 19.3  
-- Added workaround for the hover popup issue (hiding below autohidden panel)  
-- Reorganized the Settings dialog  

![compact interface screenshot](./extra/down+up_speed_compact+tooltip.png) 
## Features
* Shows current download and upload speed
* Shows a hover popup with total amount of data downloaded and uploaded OR  
	alternatively shows a tooltip (when hover popup is disabled)
* Opens a terminal with list of current Internet connections on a left mouse click.  
  Closes the terminal on the next mouse click.
* Shows data limit usage
  * Displays a percentage of data limit usage as a circle or text
  * Invokes a command when the data limit exceeded
* Customizable
  * Two GUI types
  * Show average speed per second or amount of data transferred
  * Display values as multiples of bytes or bits
  * Enable or disable a hover popup
  * Customize a command used to list current Internet connections

## Installation
1. Extract .zip archive to ~/.local/share/cinnamon/applets
2. Enable the applet in Cinnamon settings

## Usage
To specify a network interface:  
  
1. Right click on the applet
2. From "Network interfaces" submenu check a network interface
